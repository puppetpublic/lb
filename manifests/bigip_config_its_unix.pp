# Create a directory to hold the bigip configuration files and put the
# bigip credentials for the ITS-UNIX partition in it.

class lb::bigip_config_its_unix {
  file { '/etc/bigip':
    ensure => directory,
    mode   => '0755',
  }
  base::wallet { "idg-woz-lb-password-its-unix":
    path    => '/etc/bigip/bigip.password',
    type    => file,
    require => File['/etc/bigip'];
  }
}
