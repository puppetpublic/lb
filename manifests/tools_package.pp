# Install the lb-tools package that has utilities configured by both
# the lb::signal and lb::bigip defines.

class lb::tools_package {
    package {'stanford-lb-tools': ensure => present}
}
