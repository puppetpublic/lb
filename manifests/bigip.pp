# Viewing and controlling an F5 BigIP hardware load balancer.
#
# This module can be used to install in two modes:
#
#  * Install load balancer remctl status displays only.  If the
#    signal_port is not specified then only the display remctl
#    support will be configured.
#
#  * Install load balancer remctl pool membership control and
#    load balancer status displays.
#
# The most common useage is expected to be to support a single service
# with both pool membership control and pool status displays.
# Minimually only the signal_port and ensure parameters need be
# installed.  But, it is recommended practice to specify the
# bigip_service parameter as well because it will greatly speed up the
# 'show member' display by limiting the number of virtual servers that
# need to be parsed.
#
# The control commands require that the hardware load balancer be
# configured with a health check that checks status on an alternate port
# from the main services.  See the wiki under the Tools section for
# configuration documentation.
#
# Control and status example:
#
#  The following puppet fragment from the ldap-dev replica manifest
#  installs the ldap remctl command that supports the add, remove,
#  yank, show, and status commands.
#
#    lb::bigip { 'ldap':
#        ensure        => present,
#        bigip_service => "ldap-dev",
#        signal_port   => 8389
#    }
#    
# Status example:
#
#  The following puppet fragment from the tools-dev manifest
#  installs the bigip remctl command that supports the show command.
#
#    lb::bigip { 'bigip': ensure => present }
#

define lb::bigip(
  $ensure,
  $bigip_service   = 'ALL',
  $signal_port     = 'NONE',
  $remctl_acl      = '/etc/remctl/acl/systems',
  $remctl_cmd      = 'NONE',
  $bigip_name      = 'woz-lb-new.stanford.edu',
  $bigip_partition = 'ITS-UNIX',
  $xinetd          = 'NONE'
) {
  $bigip_conf     = "/etc/bigip/${name}.conf"
  $bigip_help     = "/etc/bigip/${name}.help"
  $bigip_wrapper  = "/usr/bin/bigip-$name"

  case $remctl_cmd {
    'NONE':  { $rem_cmd = $name       }
    default: { $rem_cmd = $remctl_cmd }
  }
  $remctl_conf    = "/etc/remctl/conf.d/$rem_cmd"

  # These will not get use if $signal_port is not set
  $signal_file      = "/etc/no$name"
  $signal_log       = "/var/log/${name}-signal"
  $signal_wrapper   = "/usr/bin/signal-$name"
  $xinetdFile       = "/etc/xinet.d/sulb-$name"

  case $ensure {
    'absent': {
      file {
        "$bigip_conf":     ensure => absent;
        "$bigip_help":     ensure => absent;
        "$bigip_wrapper":  ensure => absent;
        "$remctl_conf":    ensure => absent;
      }
      case $signal_port {
        'NONE':  { }
        default: {
          if    (($::operatingsystem == 'Debian') and ($::lsbmajdistrelease >= 8))
             or (($::operatingsystem == 'Ubuntu') and ($::lsbmajdistrelease >= 15))
             or (    (   ($::operatingsystem == 'RedHat')
                      or ($::operatingsystem == 'CentOS'))
                  and ($::lsbmajdistrelease >= 7)
             )
          {
            file { "/lib/systemd/system/signal-${name}.service":
              ensure  => absent,
              require => Service["signal-${name}"],
            }
            service { "signal-${name}":
              ensure  => 'stopped',
              enable  => false,
            }
          }
          base::daemontools::supervise {"$name":  ensure => absent }
          file {
            "$signal_wrapper": ensure => absent;
            "$xinetdFile":     ensure => absent;
          }
        }
      }
    }
    'present': {
      include lb::tools_package
      case $signal_port {
        'NONE':  {
          $remctl_conf_template = 'lb/bigip-remctl-conf.erb'
          $bigip_conf_template  = 'lb/bigip.conf.erb'
        }
        default: {
          $remctl_conf_template = 'lb/bigip-signal-remctl-conf.erb'
          $bigip_conf_template  = 'lb/bigip-signal.conf.erb'
          base::iptables::rule {"signal-${signal_port}":
            ensure => present,
            description => 'Hardware Load Balancer Health Check',
            port        => $signal_port,
            protocol    => 'tcp',
            target      => 'ACCEPT',
          }
          file {
            "$signal_wrapper":
              ensure  => present,
              mode    => '0755',
              content => template('lb/bigip-signal-wrapper.erb');
            "$bigip_help":
              mode    => '0644',
              content => template('lb/bigip-signal.help.erb'),
              require => File['/etc/bigip'];
          }
          case $xinetd {
            'NONE': {
              if    (($::operatingsystem == 'Debian') and ($::lsbmajdistrelease >= 8))
                 or (($::operatingsystem == 'Ubuntu') and ($::lsbmajdistrelease >= 15))
                 or (    (   ($::operatingsystem == 'RedHat')
                          or ($::operatingsystem == 'CentOS'))
                      and ($::lsbmajdistrelease >= 7)
                 )
              {
                file { "/lib/systemd/system/signal-${name}.service":
                  ensure  => present,
                  require => Package['stanford-lb-tools'],
                  content => template('lb/bigip-signal-run-systemd.erb'),
                }
                exec { "signal_${name}_restart_systemd":
                  command     => 'systemctl daemon-reload',
                  subscribe   => File["/lib/systemd/system/signal-${name}.service"],
                  before      => Service["signal-${name}"],
                  refreshonly => true,
                }
                service { "signal-${name}":
                  ensure  => 'running',
                  enable  => true,
                  require => File["/lib/systemd/system/signal-${name}.service"],
                }
                base::daemontools::supervise {"$name":
                  ensure => absent,
                }
              } else {
                base::daemontools::supervise {"$name":
                  ensure  => present,
                  content => template('lb/bigip-signal-run.erb');
                }
              }
            }
            default: {
              # xinetd configuration
              if    (($::operatingsystem == 'Debian') and ($::lsbmajdistrelease >= 8))
                 or (($::operatingsystem == 'Ubuntu') and ($::lsbmajdistrelease >= 15))
                 or (    (   ($::operatingsystem == 'RedHat')
                          or ($::operatingsystem == 'CentOS'))
                      and ($::lsbmajdistrelease >= 7)
                 )
              {
                file { "/lib/systemd/system/signal-${name}.service":
                  ensure  => absent,
                  require => Service["signal-${name}"],
                }
                service { "signal-${name}":
                  ensure  => 'stopped',
                  enable  => false,
                }
              }
              base::daemontools::supervise {"$name": ensure => absent }
              base::xinetd::config { "sulb-${signal_port}":
                server      => '/usr/sbin/lbSignal',
                server_args => "$signal_file",
                description => 'Load Balancer Health Check',
                server_type => 'UNLISTED',
                port        => $signal_port,
                cps         => '50 5',
                env         => 'PATH=/sbin:/bin:/usr/sbin:/usr/bin',
                log_type    => 'NONE',
                require     => Package['stanford-lb-tools'],
              }
            }
          }
        }
      }
      # Pull in the configuration required to access the bigip
      # load balancer using SOAP calls.
      case $bigip_partition {
        'ITS-UNIX': { include lb::bigip_config_its_unix }
        'SNSR':     { include lb::bigip_config_snsr     }
      }
      file {
        "$bigip_conf":
          mode    => '0644',
          content => template($bigip_conf_template),
          require => File['/etc/bigip'];
        "$remctl_conf":
          ensure  => present,
          mode    => '0644',
          content => template("$remctl_conf_template"),
          require => File['/etc/bigip'];
        "$bigip_wrapper":
          ensure  => present,
          mode    => '0755',
          content => template('lb/bigip-wrapper.erb');
      }
    }
  }
}
