# Create a directory to hold the bigip configuration files and
# put the bigip credentials for the SNSR paritition in it.

class lb::bigip_config_snsr {
  file { '/etc/bigip':
    ensure => directory,
    mode   => '0755',
  }
  base::wallet { 'unix-woz-lb-password-its-snsr':
    path    => '/etc/bigip/bigip.password',
    type    => file,
    require => File['/etc/bigip'];
  }
}
