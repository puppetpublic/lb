# NOTE: lb::signal is depreciated in favor or lb::bigip
#
# Set up the supervise job and remctl commands to allows hardware load
# balanced host's pool membership to be control from the command line.
#
#
# Examples:
#
#     # Create a signal listener that listens on port 9080,
#     # uses /etc/nostanfordwho as a signal file, and
#     # stanfordwho as the remctl command.
#     lb::signal { "stanfordwho":
#         port          => 9080,
#         signal        => '/etc/nostanfordwho',
#         log           => '/var/log/stanfordwho-signal',
#         remctl        => 'stanfordwho',
#         remctlACL     => '/etc/remctl/acl/group1 /etc/remctl/acl/group2',
#     }
#

define lb::signal(
  $ensure        = 'present',
  $port          = 8389,
  $signal        = 'NONE',
  $log           = 'NONE',
  $remctl        = 'NONE',
  $remctlACL     = '/etc/remctl/acl/systems',
  $xinetd        = 'NONE',
  $ipv6          = false
) {

  # Common variables
  $signalPort  = "$port"
  $remctlShell = "/usr/bin/signal-$name"
  case $signal {
    'NONE':  { $signalFile = "/etc/no$name" }
    default: { $signalFile = "$signal"      }
  }
  case $log {
    'NONE':  { $signalLog  = "/var/log/${name}-signal" }
    default: { $signalLog  = "$log"                    }
  }
  case $remctl {
    'NONE':  { $remctlFile = "/etc/remctl/conf.d/$name"   }
    default: { $remctlFile = "/etc/remctl/conf.d/$remctl" }
  }
  $xinetdFile = "/etc/xinet.d/sulb-$name"

  case $ensure {
    'absent': {
      base::daemontools::supervise {"$name": ensure => absent }
      file {
        "$remctlFile":  ensure => absent;
        "$remctlShell": ensure => absent;
        "$xinetdFile":  ensure => absent;
      }
    }
    'present': {
      include lb::tools_package
      base::iptables::rule {"signal-${port}":
        ensure => present,
        description => 'Hardware Load Balancer Health Check',
        port        => $port,
        protocol    => 'tcp',
        target      => 'ACCEPT',
      }
      file {"$remctlFile":
        ensure  => present,
        mode    => '0644',
        content => template('lb/signal-remctl.erb'),
        require => Package['remctl-server'],
      }
      file {"$remctlShell":
        ensure  => present,
        mode    => '0755',
        content => template('lb/signal-remctl-shell.erb');
      }
      case $xinetd {
        'NONE': {
          # Legacy signal support using customer server
          base::daemontools::supervise {"$name":
            ensure  => present,
            content => template('lb/signal-run.erb');
          }
        }
        default: {
          # xinetd configuration
          base::daemontools::supervise {"$name": ensure => absent }
          base::xinetd::config { "sulb-${port}":
            server      => '/usr/sbin/lbSignal',
            server_args => "$signalFile",
            description => 'Load Balancer Health Check',
            server_type => 'UNLISTED',
            port        => "${port}",
            flags       => $ipv6 ? { true => 'IPv6', false => undef },
            cps         => '50 5',
            env         => 'PATH=/sbin:/bin:/usr/sbin:/usr/bin',
            require     => Package['stanford-lb-tools'],
          }
        }
      }
    }
  }
}
